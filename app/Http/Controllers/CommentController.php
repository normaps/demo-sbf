<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function index() {
        $comments = Comment::all();
        return view('index', ['comments' => $comments]);
    }

    public function add(Request $request) {
        $comment = new Comment;
        $comment->name = $request->input('nama');
        $comment->content = $request->input('description');
        $comment->save();    
        return redirect('/comments');
    }
}
